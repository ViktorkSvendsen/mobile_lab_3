package com.example.myapplication;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity implements SensorEventListener{
    private TextView xText, highscoreText;
    //final MediaPlayer levelUp = MediaPlayer.create(this,R.raw.smb_powerup);
    private long acc;   // used for total acceleration
    private long highScore = 20; // sets highscore default to 20
    private long currentScore, iterations; //score of last throw & iterations used to reset
    private Sensor mySensor;
    private SensorManager sm;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //create our sensor manager
        sm = (SensorManager)getSystemService(SENSOR_SERVICE);

        //acceleromator sensor

        mySensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // register sensor listeneer
        sm.registerListener(this, mySensor, SensorManager.SENSOR_DELAY_NORMAL);

        //assign TextView
        xText = (TextView)findViewById(R.id.xText);
        highscoreText = (TextView)findViewById(R.id.highscoreText);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        acc = Math.round(Math.sqrt(event.values[0]*event.values[0] + event.values[1]*event.values[1] + event.values[2]*event.values[2]) -SensorManager.GRAVITY_EARTH);
        //updates regularly to 0 because of common "noise" in the movement
        if(acc>currentScore){

            xText.setText("The ball went " + currentScore + " meters into the sky!");
            currentScore = acc;

        }
        //self explanatory, only updates highscore if the current acceleration was greater
        if (acc>highScore){
            highscoreText.setText("Your highscore is : " + acc + " meters!");
            //levelUp.start();
            highScore = acc;
        }

        //just gives us a little delay to reset the last throw due to movement
        iterations++;
        if(iterations>250){
        iterations = 0;
        currentScore = 0;
        }

    }




    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}
