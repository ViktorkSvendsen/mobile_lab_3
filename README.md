Creator - Viktor Kind Svendsen
Email 	- Viktorks@stud.ntnu.no
Phone 	- 90991275

I failed to add the sound without the app crashing, so i commented it out. It crashed already on the declaration,
but looking at Stack overflow i didnt find many alternative options. 

Theres also no graphics, but the Accelerometer works as it should! 

There is a highscore functionality and the throw resets by movement, so moving your arm up for a throw, you can see the score,
and when you take your arm back down, it should reset. 